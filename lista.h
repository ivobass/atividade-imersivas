/*
**============================================================================
** UC: 21093 - Programação por Objectos - 03
** e-fólio A  2022-23 (efolioa.cpp)
**
** Aluno: 2100927 - Ivo Baptista
** Name        : EfolioA.cpp
** Author      : Ivo Baptista
** Version     : 1.0
** Copyright   : Your copyright notice
** Description : Atividades Imersivas in C++, Ansi-style
** ===========================================================================
*/

/*Pretende-se que construa um pequeno programa para exploração simples de
atividades imersivas, guardadas com as suas três coordenadas. Cada atividade
imersiva é composta por: 1) uma string com o seu título; 2) uma string com a sua
descrição; 3) uma string com o URL de um vídeo que a apresenta; 4) três floats
que representam as coordenadas da atividade no espaço conceptual da imersão:
sistema, narrativa, agência.   */
#pragma once
#include "atividades.h"

#include <vector>

/************************************************************
    Definiçao da Clase Lista
************************************************************/
class Lista {
  /************************************************************
      Funcões da Lista
  ************************************************************/
public:
  void adicionar(Atividade valor);
  void MaxSistema() const;
  void MaxNarrativa() const;
  void MaxAgencia() const;
  void mostraMaiorSistema() const;
  void mostraMaiorNarrativa() const;
  void mostraMaiorAgencia() const;
  void ordena();
  void imprime();
  void ordenaNarrativa();
  void imprimeNarrativa();
  void ordenaAgencia();
  void imprimeAgencia();
  /************************************************************
      Criamos o Vector
  ************************************************************/
private:
  std::vector<Atividade> m_atividades;
};
