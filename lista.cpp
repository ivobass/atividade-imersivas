/*
**============================================================================
** UC: 21093 - Programação por Objectos - 03
** e-fólio A  2022-23 (efolioa.cpp)
**
** Aluno: 2100927 - Ivo Baptista
** Name        : EfolioA.cpp
** Author      : Ivo Baptista
** Version     : 1.0
** Copyright   : Your copyright notice
** Description : Atividades Imersivas in C++, Ansi-style
** ===========================================================================
*/

/*Pretende-se que construa um pequeno programa para exploração simples de
atividades imersivas, guardadas com as suas três coordenadas. Cada atividade
imersiva é composta por: 1) uma string com o seu título; 2) uma string com a sua
descrição; 3) uma string com o URL de um vídeo que a apresenta; 4) três floats
que representam as coordenadas da atividade no espaço conceptual da imersão:
sistema, narrativa, agência.   */
#include <algorithm>
#include "lista.h"

/************************************************************
    Função que adiciona dados ao vector
************************************************************/
void Lista::adicionar(Atividade valor) {
  m_atividades.push_back(valor); // adiciona valor dentro do vetor
}

/*********************************************
       Calcula maior de Sistema 
**********************************************/

void Lista::MaxSistema() const {
  if (m_atividades.size() == 0) {
    return;
  }
  auto maximoSistema =
      std::max_element(std::begin(m_atividades), std::end(m_atividades));
  std::cout << "Atividade mais alta de Sistema:" << std::endl;
  (*maximoSistema).imprime();
}
/*********************************************
       Calcula maior de Narrativa 
**********************************************/
void Lista::MaxNarrativa() const {
  if (m_atividades.size() == 0) {
    return;
  }
  auto maximoNarrativa = std::max_element(
      std::begin(m_atividades), std::end(m_atividades), compareNarrativa);
  std::cout << "Atividade mais alta de Narrativa:" << std::endl;
  (*maximoNarrativa).imprimeNarrativa();
}
/*********************************************
       Calcula maior de Agencia 
**********************************************/
void Lista::MaxAgencia() const {
  if (m_atividades.size() == 0) {
    return;
  }
  auto maximoAgencia = std::max_element(std::begin(m_atividades),
                                        std::end(m_atividades), compareAgencia);
  std::cout << "Atividade mais alta de Agencia:" << std::endl;
  (*maximoAgencia).imprimeAgencia();
}

/*********************************************
       Calcula maior de Sistema
**********************************************/
void Lista::mostraMaiorSistema() const {
  if (m_atividades.size() == 0) {
    return;
  }
  auto maximoAgencia =
      std::max_element(std::begin(m_atividades), std::end(m_atividades));
  (*maximoAgencia).impMaiorSistema();
}
/*********************************************
       Calcula maior de Narrativa
**********************************************/
void Lista::mostraMaiorNarrativa() const {
  if (m_atividades.size() == 0) {
    return;
  }
  auto maximoAgencia = std::max_element(
      std::begin(m_atividades), std::end(m_atividades), compareNarrativa);
  (*maximoAgencia).impMaiorNarrativa();
}
/*********************************************
       Calcula maior de Agencia
**********************************************/
void Lista::mostraMaiorAgencia() const {
  if (m_atividades.size() == 0) {
    return;
  }
  auto maximoAgencia = std::max_element(std::begin(m_atividades),
                                        std::end(m_atividades), compareAgencia);
  (*maximoAgencia).impMaiorAgencia();
}
/************************************************************
    Função que ordena por Sistema
************************************************************/
void Lista::ordena() {
  std::sort(std::begin(m_atividades), std::end(m_atividades));
}
/************************************************************
    Função para imprimir a lista ordenada por Sistema
************************************************************/
void Lista::imprime() {
  for (const auto &valoratual : m_atividades) {
    valoratual.imprime();
  }
}
/************************************************************
    Função que ordena por Narrativa
************************************************************/
void Lista::ordenaNarrativa() {
  std::sort(std::begin(m_atividades), std::end(m_atividades), compareNarrativa);
}
/************************************************************
    Função para imprimir a lista ordenada por Narrativa
************************************************************/

void Lista::imprimeNarrativa() {
  for (const auto &valoratual : m_atividades) {
    valoratual.imprimeNarrativa();
  }
}
/************************************************************
    Função que ordena por Agencia
************************************************************/
void Lista::ordenaAgencia() {
  std::sort(std::begin(m_atividades), std::end(m_atividades), compareAgencia);
}
/************************************************************
    Função para imprimir a lista ordenada por Agencia
************************************************************/
void Lista::imprimeAgencia() {
  for (const auto &valoratual : m_atividades) {
    valoratual.imprimeAgencia();
  }
}
